<div align="center">
<p>
    <a href="https://gitlab.com/acubesat/documentation/cdr-public/-/blob/master/DDJF/DDJF_THR_ARPT.pdf">DDJF_THR 📚🧪</a> &bull;
    <a href="https://spacedot.gr/">SpaceDot 🌌🪐</a> &bull;
    <a href="https://acubesat.spacedot.gr/">AcubeSAT 🛰️🌎</a>
</p>
</div>

## Description

Here you can find our ESATAN-TMS models, together with the respective analysis results. There are two directories, `cdr/` and `summer-2022/`. `cdr/` contains the model we used in our [Critical Design Review](https://www.esa.int/Education/CubeSats_-_Fly_Your_Satellite/AcubeSAT_successfully_passes_Critical_Design_Review), while `summer-2022/` contains a more updated version. There are `README` files in both directories that you can read for additional info.

## Table of Contents

<details>
<summary>Click to expand</summary>

[[_TOC_]]

</details>

## Models

### CubeSat

```
cubesat
 ┣ v1
 ┃ ┣ model
 ┃ ┃ ┣ cdr.ere
 ┃ ┃ ┣ cdr.erg
 ┃ ┃ ┣ cdr.erk
 ┃ ┃ ┗ cdr.xml
 ┃ ┣ plots
 ┃ ┃ ┣ comms-pcb
 ┃ ┃ ┃ ┣ cold-with-passive-control.png
 ┃ ┃ ┃ ┣ cold-without-passive-control.png
 ┃ ┃ ┃ ┣ hot-with-passive-control.png
 ┃ ┃ ┃ ┗ hot-without-passive-control.png
 ┃ ┃ ┗ general
 ┃ ┃ ┃ ┣ cold-with-passive-control.png
 ┃ ┃ ┃ ┣ cold-without-passive-control.png
 ┃ ┃ ┃ ┣ hot-with-passive-control.png
 ┃ ┃ ┃ ┗ hot-without-passive-control.png
 ┃ ┗ README.md
 ┣ v2
 ┃ ┣ model
 ┃ ┃ ┣ cases
 ┃ ┃ ┃ ┣ summer-2022_Cold_11LTAN_Comissioning_Detumbling_500.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_11LTAN_Nominal_Nadir_500.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_11LTAN_Nominal_Nadir_500_Active.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_11LTAN_Nominal_Nadir_500_summer_2022.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_11LTAN_Nominal_Nadir_500_v2.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_11LTAN_Nominal_Nadir_500_v3.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_11LTAN_Nominal_Nadir_500_v4.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_6LTAN_Comissioning_Detumbling_500.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_6LTAN_Nominal_Nadir_500.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_6LTAN_Nominal_Nadir_500_Active.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_6LTAN_Nominal_Nadir_500v1_1.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_Nominal_Glass_PEEK_Final.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_Nominal_TIME_STEP_005.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_Nominal_TIME_STEP_RAD_10.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_Science_11LTAN_Final.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_Science_11LTAN_Finalv1_1.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_Science_11LTAN_Finalv1_1_summer_2022.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_Science_11LTAN_Finalv_2.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_Science_11LTAN_Finalv_3.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_Science_11LTAN_Finalv_4.tpl
 ┃ ┃ ┃ ┣ summer-2022_Cold_Science_6LTAN_Final.tpl
 ┃ ┃ ┃ ┣ summer-2022_Hot_6LTAN_Nominal_Nadir_600.tpl
 ┃ ┃ ┃ ┣ summer-2022_Hot_Science_6LTAN_600.tpl
 ┃ ┃ ┃ ┣ summer-2022_Hot_Science_6LTAN_Final.tpl
 ┃ ┃ ┃ ┗ summer-2022_Hot_Science_6LTAN_Nadir.tpl
 ┃ ┃ ┣ summer-2022.ere
 ┃ ┃ ┣ summer-2022.erg
 ┃ ┃ ┣ summer-2022.erk
 ┃ ┃ ┗ summer-2022.xml
 ┃ ┗ README.md
 ┣ v3
 ┃ ┗ geometry.erg
 ┗ v4
 ┃ ┗ geometry.erg
```
- v1: Circa CDR
- v2: Circa summer 2022
- v3: Circa late 2022
- v4: Current

### Fluidic System

### TVAC

### Manifold Analytic TMM